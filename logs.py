import logging
import os
import sys

# Указываем формат логов
access_format = logging.Formatter(
    '(%(asctime)s), %(message)s'
)
system_format = logging.Formatter(
    '%(asctime)s - %(levelname)s - %(message)s'
)

logs_dir = '/var/crud/logs/'
app_access_path = f'{logs_dir}app_access.log'
app_error_path = f'{logs_dir}app_error.log'

if (not os.path.exists(app_access_path)):
    os.system('mkdir -p /var/crud/logs/')
    with open(app_access_path, "w") as file:
        file.write("Created app_access.log file...")
        file.close()

if (not os.path.exists(app_error_path)):
    os.system('mkdir -p /var/crud/logs/')
    with open(app_error_path, "w") as file:
        file.write("Created app_error.log file...")
        file.close()

# Ставим логгер
def setup_logger(name, stream, formatter, level=logging.INFO):
    handler = logging.FileHandler(stream)
    handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)
    return logger


access_logger = setup_logger(
    'access_logger',
    '/var/crud/logs/app_access.log',
    access_format
)
error_logger = setup_logger(
    'error_logger',
    '/var/crud/logs/app_error.log',
    access_format,
    level=logging.ERROR
)
system_logger = setup_logger(
    'system_error',
    '/var/crud/logs/app_error.log',
    system_format,
    level=logging.ERROR
)
