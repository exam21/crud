from flask import Flask, request, jsonify
from psycopg2 import errors
import psycopg2
import hashlib
import jwt
import os
import socket
import sql_requests
import logs

SECRET_KEY = os.getenv("SECRET_KEY")
DB_PASS = os.getenv("DB_PASS")
DB_HOST = os.getenv("DB_HOST")
ANAME = os.getenv("ANAME")

connect = psycopg2.connect(
    f"dbname=cars user=cars_user password={DB_PASS} host={DB_HOST} port=5432")
cursor = connect.cursor()

app = Flask(__name__)

@app.route('/')
def main():
    ip = socket.gethostbyname(socket.gethostname())
    return f"{ANAME} : {ip}"

# AUTHORIZATION
@app.route('/auth/login/', methods=["POST"])
def auth():
    # {"user", "password"}
    #
    body = request.get_json()
    user = body["user"]
    password = body["password"]
    encode_password = hashlib.md5(f"{password}{SECRET_KEY}".encode()).hexdigest()
    
    cursor.execute(sql_requests.sql_auth(user, encode_password))
    data = cursor.fetchone()
    
    try:
        user_id, name = data
    except TypeError:
        logs.error_logger.error(f"🔴 Access denied for {request.remote_addr}")
        return 'Access denied', 403
    
    encoded_jwt = jwt.encode({"name": name, "user_id": user_id}, 
                             SECRET_KEY, 
                             algorithm="HS256")
    
    logs.access_logger.info(f"🟢 {request.remote_addr} successfully logged in")
    return jsonify({"access_token":encoded_jwt})

# INSERT
@app.route('/cars/', methods=["POST"])
def insert():
    # Authorization check
    try:
        user_token = request.headers.get("Authorization")  # Bearer
        auth_type, token = user_token.split(" ")
    except (KeyError, ValueError, AttributeError):
        logs.error_logger.error(f"🔴 Access denied for {request.remote_addr}")
        return 'Access denied', 403
    user_data = jwt.decode(token, 
                           SECRET_KEY, 
                           algorithms=["HS256"])
    
    body = request.get_json()
    try:
        cursor.execute(sql_requests.sql_insert(body))
    except (errors.UniqueViolation):
        logs.error_logger.error(f"🔴 {request.remote_addr}: bad order (Not unique value)")
        return 'Not unique value', 400    
        
    connect.commit()
    logs.access_logger.info(f"🟢 {request.remote_addr}: insert data:\n{body}")
    return '', 201

# GET
@app.route('/cars/', methods=["GET"])
def fetch_all():
    cars = []
    cursor.execute(sql_requests.sql_get())
    data = cursor.fetchall()
    
    for row in data:
        car_id, car_name, car_country, car_auto_kpp = row
        cars.append({
            "id": car_id,
            "name": car_name,
            "country": car_country,
            "auto_kpp": car_auto_kpp
        })
    logs.access_logger.info(f"🟢 {request.remote_addr}: successful get data")
    return jsonify({"data": cars})

# RETRIEVE
@app.route('/cars/<car_id>/', methods=["GET"])
def fetch(car_id):
    cursor.execute(sql_requests.sql_retrieve(car_id))
    data = cursor.fetchone()
    try:
        car_id, car_name, car_country, car_auto_kpp = data
    except TypeError:
        logs.access_logger.info(f"🟢 {request.remote_addr}: successful get data")
        return 'Not found', 404
    
    logs.access_logger.info(f"🟢 {request.remote_addr}: successful retrive data")
    return jsonify({"data": {
        "id": car_id,
        "name": car_name,
        "country": car_country,
        "auto_kpp": car_auto_kpp
    }})
    
# UPDATE
@app.route('/cars/<car_id>/', methods=["POST"])
def update(car_id):
    # {key:value}
    # Authorization check
    try:
        user_token = request.headers.get("Authorization")  # Bearer
        auth_type, token = user_token.split(" ")
    except (KeyError, ValueError, AttributeError):
        logs.error_logger.error(f"🔴 Access denied for {request.remote_addr}")
        return 'Access denied', 403
    user_data = jwt.decode(token, 
                           SECRET_KEY,
                           algorithms=["HS256"])
    
    body = request.get_json()
    cursor.execute(sql_requests.sql_update(car_id, body))
    connect.commit()
    
    logs.access_logger.info(f"🟢 {request.remote_addr}: successful update data")
    return '', 201

# DELETE
@app.route('/cars/<car_id>/', methods=["DELETE"])
def delete(car_id):
    # Authorization check
    try:
        user_token = request.headers.get("Authorization")  # Bearer
        auth_type, token = user_token.split(" ")
    except (KeyError, ValueError, AttributeError):
        logs.error_logger.error(f"🔴 Access denied for {request.remote_addr}")
        return 'Access denied', 403
    
    user_data = jwt.decode(token, 
                           SECRET_KEY,
                           algorithms=["HS256"])
    
    cursor.execute(sql_requests.sql_delete(car_id))
    connect.commit()
    logs.access_logger.info(f"🟢 {request.remote_addr}: successful delete data")
    return '', 204

if __name__ == '__main__':
    app.run(host='0.0.0.0')